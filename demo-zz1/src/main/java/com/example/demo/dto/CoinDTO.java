package com.example.demo.dto;

import com.example.demo.domain.Coin;
import com.example.demo.util.StringUtil;

import java.text.DecimalFormat;

public class CoinDTO {
    private String id;
    private String name;
    private String baseAsset;
    private String quoteAsset;
    private String lastPrice;
    private String volumn24h;
    private String createdAt;
    private String updatedAt;
    private String status;
    private String marketId;

    public CoinDTO() {
    }

    public CoinDTO(Coin coin) {
        this.id = String.valueOf(coin.getId());
        this.name = coin.getName();
        this.baseAsset = coin.getBaseAsset();
        this.quoteAsset = coin.getQuoteAsset();
        this.lastPrice = new DecimalFormat("$##.##").format(coin.getLastPrice());
        this.volumn24h = new DecimalFormat("$##.##").format(coin.getVolumn24h());
        this.createdAt = new StringUtil().convertMilToString(coin.getCreatedAt());
        this.updatedAt = new StringUtil().convertMilToString(coin.getUpdatedAt());
        this.status = new StringUtil().convertIntStatusToString(coin.getStatus());
        this.marketId = String.valueOf(coin.getMarketId());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBaseAsset() {
        return baseAsset;
    }

    public void setBaseAsset(String baseAsset) {
        this.baseAsset = baseAsset;
    }

    public String getQuoteAsset() {
        return quoteAsset;
    }

    public void setQuoteAsset(String quoteAsset) {
        this.quoteAsset = quoteAsset;
    }

    public String getLastPrice() {
        return lastPrice;
    }

    public void setLastPrice(String lastPrice) {
        this.lastPrice = lastPrice;
    }

    public String getVolumn24h() {
        return volumn24h;
    }

    public void setVolumn24h(String volumn24h) {
        this.volumn24h = volumn24h;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMarketId() {
        return marketId;
    }

    public void setMarketId(String marketId) {
        this.marketId = marketId;
    }
}
