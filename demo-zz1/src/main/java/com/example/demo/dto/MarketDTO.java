package com.example.demo.dto;

import com.example.demo.domain.Market;
import com.example.demo.util.StringUtil;

public class MarketDTO {
    private String id;
    private String name;
    private String description;
    private String createdAt;
    private String updatedAt;
    private String status;

    public MarketDTO() {
    }

    public MarketDTO(Market market) {
        this.id = String.valueOf(market.getId());
        this.name = market.getName();
        this.description = market.getDescription();
        this.createdAt = new StringUtil().convertMilToString(market.getCreatedAt());
        this.updatedAt = new StringUtil().convertMilToString(market.getUpdatedAt());
        this.status = new StringUtil().convertIntStatusToString(market.getStatus());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
