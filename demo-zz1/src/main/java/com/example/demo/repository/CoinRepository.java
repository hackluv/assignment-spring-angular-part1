package com.example.demo.repository;

import com.example.demo.domain.Coin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CoinRepository extends JpaRepository<Coin, Long> {
    List<Coin> getAllByStatus(int status);
    List<Coin> getAllByMarketIdAndStatus(long id, int status);
    List<Coin> getCoinByNameAndStatus(String name, int status);
}
