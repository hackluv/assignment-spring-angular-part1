package com.example.demo.domain;

import com.example.demo.domain.enum1.Status;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Calendar;

@Entity
public class Coin {
    @Id
    private long id;
    private String name;
    private String baseAsset;
    private String quoteAsset;
    @Column(name = "last_asset", columnDefinition="Decimal(10,2) default '100.00'")
    private double lastPrice;
    @Column(name = "volumn24h", columnDefinition="Decimal(10,2) default '100.00'")
    private double volumn24h;
    private long createdAt;
    private long updatedAt;
    private int status;
    private long marketId;

    public Coin() {
        this.id = Calendar.getInstance().getTimeInMillis();
        this.createdAt = Calendar.getInstance().getTimeInMillis();
        this.updatedAt = Calendar.getInstance().getTimeInMillis();
        this.status = Status.ACTIVE.getValue();
    }

    public Coin(long id, String name, String baseAsset, String quoteAsset, double lastPrice, double volumn24h, long createdAt, long updatedAt, int status, long marketId) {
        this.id = id;
        this.name = name;
        this.baseAsset = baseAsset;
        this.quoteAsset = quoteAsset;
        this.lastPrice = lastPrice;
        this.volumn24h = volumn24h;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.status = status;
        this.marketId = marketId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBaseAsset() {
        return baseAsset;
    }

    public void setBaseAsset(String baseAsset) {
        this.baseAsset = baseAsset;
    }

    public String getQuoteAsset() {
        return quoteAsset;
    }

    public void setQuoteAsset(String quoteAsset) {
        this.quoteAsset = quoteAsset;
    }

    public double getLastPrice() {
        return lastPrice;
    }

    public void setLastPrice(double lastPrice) {
        this.lastPrice = lastPrice;
    }

    public double getVolumn24h() {
        return volumn24h;
    }

    public void setVolumn24h(double volumn24h) {
        this.volumn24h = volumn24h;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public long getMarketId() {
        return marketId;
    }

    public void setMarketId(long marketId) {
        this.marketId = marketId;
    }
}
