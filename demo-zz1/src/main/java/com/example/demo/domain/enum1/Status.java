package com.example.demo.domain.enum1;

public enum Status {
    DELETED(-1), DEACTIVE(0), ACTIVE(1);

    int value;

    Status(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public Status findByValue(int value){
        for (Status status : Status.values()){
            if (status.getValue() == value){
                return status;
            }
        }
        return null;
    }
}
