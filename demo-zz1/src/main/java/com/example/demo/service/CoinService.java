package com.example.demo.service;

import com.example.demo.domain.Coin;
import com.example.demo.domain.enum1.Status;
import com.example.demo.repository.CoinRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CoinService {

    @Autowired
    CoinRepository coinRepository;

    public List<Coin> getList() {
        return coinRepository.getAllByStatus(Status.ACTIVE.getValue());
    }

    public Coin create(Coin coin) {
        coinRepository.save(coin);
        return coin;
    }

    public List<Coin> searchByMarket(long id) {
        return coinRepository.getAllByMarketIdAndStatus(id, Status.ACTIVE.getValue());
    }

    public List<Coin> getCoinByName(String name) {
        List<Coin> coinList = coinRepository.getCoinByNameAndStatus(name, Status.ACTIVE.getValue());
        return coinList;
    }
}
