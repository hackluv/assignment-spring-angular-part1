package com.example.demo.service;

import com.example.demo.domain.Market;
import com.example.demo.domain.enum1.Status;
import com.example.demo.repository.MarketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MarketService {

    @Autowired
    MarketRepository marketRepository;

    public List<Market> getList() {
        return marketRepository.getAllByStatus(Status.ACTIVE.getValue());
    }

    public Market create(Market market) {
        marketRepository.save(market);
        return market;
    }
}
