package com.example.demo.endpoint;

import com.example.demo.domain.Coin;
import com.example.demo.dto.CoinDTO;
import com.example.demo.service.CoinService;
import com.example.demo.util.JsonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/v1/coins")
@CrossOrigin
public class CoinEndpoint {

    @Autowired
    CoinService coinService;
    private JsonResponse jsonResponse = new JsonResponse();

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Object> list() {
        List<Coin> coinList = coinService.getList();
        List<CoinDTO> coinDTOList = new ArrayList<>();

        for (Coin coin : coinList) {
            CoinDTO coinDTO = new CoinDTO(coin);
            coinDTOList.add(coinDTO);
        }
        return new ResponseEntity<>(jsonResponse.setStatus(HttpStatus.OK.value()).setMessage("Get List OK").setData(coinDTOList), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Object> store(@RequestBody CoinDTO coinDTO) {
        Coin coin = new Coin();
        coin.setName(coinDTO.getName());
        coin.setBaseAsset(coinDTO.getBaseAsset());
        coin.setQuoteAsset(coinDTO.getQuoteAsset());
        coin.setLastPrice(Double.parseDouble(coinDTO.getLastPrice()));
        coin.setVolumn24h(Double.parseDouble(coinDTO.getVolumn24h()));
        coin.setMarketId(Long.parseLong(coinDTO.getMarketId()));
        Coin coin1 = coinService.create(coin);
        return new ResponseEntity<>(jsonResponse.setStatus(HttpStatus.CREATED.value()).setMessage("Create Success").setData(new CoinDTO(coin1)), HttpStatus.CREATED);
    }

    @RequestMapping(value = "search", method = RequestMethod.GET)
    public ResponseEntity<Object> searchByMarId(@RequestParam(name = "marketId") String id, @RequestParam(name = "name") String name) {
        List<Coin> coinList = new ArrayList<>();
        long parseId = Long.parseLong(id);
        List<CoinDTO> coinDTOList = new ArrayList<>();
        if (parseId == 0 && (name.length() == 0 || name == null)) {
            coinList = coinService.getList();
            for (Coin coin : coinList) {
                CoinDTO coinDTO = new CoinDTO(coin);
                coinDTOList.add(coinDTO);
            }
            return new ResponseEntity<>(jsonResponse.setStatus(HttpStatus.OK.value()).setMessage("Get List OK").setData(coinDTOList), HttpStatus.OK);
        } else if (parseId != 0 && name.length() == 0) {
            coinList = coinService.searchByMarket(parseId);
            if (coinList == null) {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            } else {
                for (Coin coin : coinList) {
                    CoinDTO coinDTO = new CoinDTO(coin);
                    coinDTOList.add(coinDTO);
                }
                return new ResponseEntity<>(jsonResponse.setStatus(HttpStatus.OK.value()).setMessage("Search by Market OK").setData(coinDTOList), HttpStatus.OK);
            }
        } else if (parseId == 0 && (name.length() != 0 || name != null)) {
            coinList = coinService.getCoinByName(name);

            if (coinList == null) {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            } else {
                for (Coin coin : coinList) {
                    CoinDTO coinDTO = new CoinDTO(coin);
                    coinDTOList.add(coinDTO);
                }
                return new ResponseEntity<>(jsonResponse.setStatus(HttpStatus.OK.value()).setMessage("Get Coin Name").setData(coinDTOList), HttpStatus.OK);
            }
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
}
