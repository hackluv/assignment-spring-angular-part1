package com.example.demo.endpoint;

import com.example.demo.domain.Market;
import com.example.demo.dto.MarketDTO;
import com.example.demo.service.MarketService;
import com.example.demo.util.JsonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/v1/markets")
@CrossOrigin
public class MarketEndpoint {

    @Autowired
    MarketService marketService;

    JsonResponse jsonResponse = new JsonResponse();

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Object> list() {
        List<Market> marketList = marketService.getList();
        List<MarketDTO> marketDTOList = new ArrayList<>();

        for (Market market: marketList) {
            MarketDTO marketDTO = new MarketDTO(market);
            marketDTOList.add(marketDTO);
        }
        return new ResponseEntity<>(jsonResponse.setStatus(HttpStatus.OK.value()).setMessage("Get List OK").setData(marketDTOList), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Object> store(@RequestBody MarketDTO marketDTO) {
        Market market = new Market();
        market.setName(marketDTO.getName());
        market.setDescription(marketDTO.getDescription());
        marketService.create(market);
        return new ResponseEntity<>(jsonResponse.setStatus(HttpStatus.CREATED.value()).setMessage("create success").setData(new MarketDTO(market)), HttpStatus.CREATED);
    }
}
