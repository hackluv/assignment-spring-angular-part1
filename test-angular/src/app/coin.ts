export class Coin {
  // tslint:disable-next-line:max-line-length
  constructor(public name: string, public baseAsset: string, public quoteAsset, public lastPrice: string, public volumn24h: string, public marketId: string) {
  }
}
