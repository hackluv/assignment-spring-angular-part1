import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MarketListComponent } from './market-list/market-list.component';
import { MarketCreateComponent } from './market-create/market-create.component';
import {HttpClientModule} from '@angular/common/http';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CoinCreateComponent } from './coin-create/coin-create.component';
import { CoinListComponent } from './coin-list/coin-list.component';
import { CoinSearchComponent } from './coin-search/coin-search.component';

@NgModule({
  declarations: [
    AppComponent,
    MarketListComponent,
    MarketCreateComponent,
    CoinCreateComponent,
    CoinListComponent,
    CoinSearchComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
