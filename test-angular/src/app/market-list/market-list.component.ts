import {Component, OnInit} from '@angular/core';
import {Market} from '../market';
import {MarketService} from '../market.service';

@Component({
  selector: 'app-market-list',
  templateUrl: './market-list.component.html',
  styleUrls: ['./market-list.component.css']
})
export class MarketListComponent implements OnInit {
  markets: Market[];

  constructor(private marketService: MarketService) {
  }

  ngOnInit() {
    this.marketService.listLive().subscribe({
      next: value => {
        this.markets = value;
      }
    });
  }

}
