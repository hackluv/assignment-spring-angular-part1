import {Component, OnInit} from '@angular/core';
import {Coin} from '../coin';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MarketService} from '../market.service';
import {CoinService} from '../coin.service';
import {of} from 'rxjs';

@Component({
  selector: 'app-coin-create',
  templateUrl: './coin-create.component.html',
  styleUrls: ['./coin-create.component.css']
})
export class CoinCreateComponent implements OnInit {
  markets: any = [];
  coin: Coin;
  form: FormGroup;
  formControls = {
    name: [null],
    baseAsset: [null],
    quoteAsset: [null],
    lastPrice: [null],
    volumn24h: [null],
    marketId: [null]
  };

  constructor(private formBuilder: FormBuilder, private marketService: MarketService, private coinService: CoinService) {
  }

  ngOnInit() {
    this.form = this.formBuilder.group(this.formControls);
    this.markets = this.getMarket();
  }

  getMarket() {
    this.marketService.listLive().subscribe(params => {
      this.markets = params;
    });
  }

  doSubmit() {
    this.coinService.saveLive(this.form.value).subscribe();
  }
}
