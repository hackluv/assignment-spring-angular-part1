import {Injectable} from '@angular/core';
import {Coin} from './coin';
import {HttpClient} from '@angular/common/http';
import {Market} from './market';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CoinService {
  coins: Coin[] = [];

  constructor(private http: HttpClient) {

  }

  list(): Coin[] {
    return this.coins;
  }

  create(coin: Coin) {
    this.coins.push(coin);
  }

  listLive(): Observable<Coin[]> {
    return this.http.get<{ data: Coin[] }>(
      'http://localhost:8080/api/v1/coins',
      {
        headers: {
          'Content-Type': 'application/json'
        }
      }
    ).pipe(map(({data}) => data));
  }

  saveLive(coin: Coin): Observable<Coin> {
    return this.http.post<{ data: Coin }> (
      'http://localhost:8080/api/v1/coins',
      coin,
      {
        headers: {
          'Content-Type': 'application/json'
        }
      }
    ).pipe(map(({data}) => data ));
  }
}
