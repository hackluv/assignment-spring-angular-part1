import {Component, OnInit} from '@angular/core';
import {Market} from '../market';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MarketService} from '../market.service';

@Component({
  selector: 'app-market-create',
  templateUrl: './market-create.component.html',
  styleUrls: ['./market-create.component.css']
})
export class MarketCreateComponent implements OnInit {
  market: Market;
  form: FormGroup;
  formControls = {
    name: [null],
    description: [null]
  };

  constructor(private formBuilder: FormBuilder, private marketService: MarketService) {
  }

  ngOnInit() {
    this.form = this.formBuilder.group(this.formControls);
  }

  doSubmit() {
    this.marketService.saveLive(this.form.value).subscribe();
  }
}
