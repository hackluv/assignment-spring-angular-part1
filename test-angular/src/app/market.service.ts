import {Injectable} from '@angular/core';
import {Market} from './market';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MarketService {
  markets: Market[] = [];

  constructor(private http: HttpClient) {

  }

  list(): Market[] {
    return this.markets;
  }

  create(market: Market) {
    this.markets.push(market);
  }

  listLive(): Observable<Market[]> {
    return this.http.get<{ data: Market[] }>(
      'http://localhost:8080/api/v1/markets',
      {
        headers: {
          'Content-Type': 'application/json'
        }
      }
    ).pipe(map(({data}) => data));
  }

  saveLive(market: Market): Observable<Market> {
    return this.http.post<{ data: Market }> (
      'http://localhost:8080/api/v1/markets',
      market,
      {
        headers: {
          'Content-Type': 'application/json'
        }
      }
    ).pipe(map(({data}) => data ));
  }
}


