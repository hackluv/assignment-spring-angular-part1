import { Component, OnInit } from '@angular/core';
import {Market} from '../market';
import {MarketService} from '../market.service';
import {Coin} from '../coin';
import {CoinService} from '../coin.service';

@Component({
  selector: 'app-coin-list',
  templateUrl: './coin-list.component.html',
  styleUrls: ['./coin-list.component.css']
})
export class CoinListComponent implements OnInit {

  coins: Coin[];

  constructor(private coinService: CoinService) {
  }

  ngOnInit() {
    this.coinService.listLive().subscribe({
      next: value => {
        this.coins = value;
      }
    });
  }

}
